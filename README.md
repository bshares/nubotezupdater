# NuBot EZ-Updater

Sorta-mirror for easy upgrading of NuBot on remote servers using git branches for each binary released by the NuBot development team.

> WARNING! Use at your own risk! If you would prefer to compile NuBot from source, please visit https://bitbucket.org/JordanLeePeershares/nubottrading, or https://bitbucket.org/JordanLeePeershares/nubottrading/downloads for direct access to the pre-compiled binaries.

## Current Version Details

Binary: NuBot 0.1.4c ([Compiled](https://bitbucket.org/JordanLeePeershares/nubottrading/downloads/nubot-v0.1.4c.zip) / [Source](https://bitbucket.org/JordanLeePeershares/nubottrading/branch/hotfix/0.1.4c))

Post-Release Resource Patches:

* New keystore.jks file with updated SSL certificate for Coinbase ([nubottrading/1c2a01ce9c59](https://bitbucket.org/JordanLeePeershares/nubottrading/commits/1c2a01ce9c59/)

## Instructions:

On the server or local machine that you would like to run NuBot on, run these console commands:

```
cd /path/to/directory/where/you/will/run/your/nubot(s)

# Clone this repository
git clone https://bitbucket.org/bshares/nubotezupdater.git

# Rename the repository you just created with the exchange and marketpair
# you plan to support with that instance of NuBot (ex. 'ccedk_nbtbtc').
# If you use Raportisto to collect data and generate a public report,
# make sure you use the correct directory naming syntax, exchange_marketpair

mv nubotezupdater exchange_marketpair

cd exchange_marketpair
```

The **master** branch of NuBot EZ-Updater will always contain the most recent set of stable binaries released by the NuBot development team, in addition to any resource patches that may have come out since then. If you would like to install an earlier version of NuBot, you can check out the branch that matches the name of the compiled binary version you would like to use.

```
git fetch origin

git checkout branch_name
```
